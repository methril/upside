#!/usr/bin/env python3

# Generate things from pseudocode describing states and actions in UPSide.
# 1. The action-state diagram for the design document
# 2. C code for the core state machine in the firmware.

class dot:
    "Output class for rendering to state diagram."
    def begin(self):
        print('strict digraph {\n    size="7.5,10";')
    def end(self):
        print('}')
    def state(self, node, text):
        print('    "{0}" [shape=oval, label="{1}"]'.format(node, text))
    def action(self, fromstate, tostate, production):
        label = "Event: %s\n%s" % (production.trigger, production.action)
        print('    "{2}" [shape=box]; {0} -> "{2}" -> {1}'.format(fromstate, tostate, label))

class firmware:
    "Output class for rendering to firmware source code."
    def __init__(self):
        self.states = []
        self.events = []
        self.actions = []
    def begin(self):
        pass
    def end(self):
        with open("transitions.h", "w") as header:
            header.write("""/*
 * transitions.h - states and actions for the UPSide firmware
 *
 * PROGRAM-GENERATED - DO NOT HAND-HACK!
 */

enum state_t {
""")
            for (i, (s, t)) in enumerate(self.states):
                header.write("    %s = %d,	/* %s */\n" % (s, i, t.replace('\\n', ' ')))
            header.write("}\n\nenum event_t {\n")
            for (i, (n, c)) in enumerate(self.events):
                header.write("    %s = %d,	/* %s */\n" % (n, i, c))
            header.write("}\n\n")
            header.write("/* action functions */\n")
            for (e, _) in self.events:
                header.write("extern void %s_action(enum state_t);\n" % e)
            header.write("extern void error(enum state_t, enum event_t);\n")
            header.write("/* end */\n")
            header.close()
        with open("transitions.c", "w") as code:
            code.write("""/*
 * transitions.c - core state machine UPSide firmware
 *
 * PROGRAM-GENERATED - DO NOT HAND-HACK!
 */
#include \"transitions.h\"

void transition(int event) {
    static int state = %s;

    switch(state) {
""" % self.states[0][0])
            for (s, t) in self.states:
                code.write("    case %s:	/* %s */\n" % (s, t))
                code.write("        switch (event) {\n")
                for (fromstate, tostate, production) in self.actions:
                    if fromstate == s:
                        code.write("            case %s:\t/* %s */\n" % (production.name, production.trigger))
                        code.write("                %s_action(%s);\n" % (production.name, fromstate))
                        code.write("                state = %s;\n" % tostate)
                        code.write("                break;\n")
                code.write("            default:\n")
                code.write("                error(%s, event);\n" % s)
                code.write("                break;\n")
                code.write("        }\n")
                code.write("        break;\n")
            code.write("    }\n}\n\n")
            code.write("/* end */\n")
    def state(self, node, text):
        self.states.append((node, text))
    def action(self, fromstate, tostate, production):
        self.actions.append((fromstate, tostate, production))
        if production.name not in self.events:
            self.events.append((production.name, production.trigger))
class production:
    def __init__(self, name, trigger, action):
        self.name = name
        self.trigger = trigger
        self.action = action

# Actions. Each action begins with a triggering event and includes
# a specification of the I2C message shipped as a notification, if any.
FOREBRAINUP = production("forebrainup",
                         r"Management daemon boots",
                         r"Alarm: STARTING")
NOCONFIG = production("noconfig",
                      r"10 seconds elapse without CONFIG message.",
                      r"Cycle power to forebrain using ENABLE\nAlarm: FAIL\n")
CHARGING = production("charging",
                      r"Mains power is on.",
                      r"Bus: Poll of midbrain reports nominal voltage.\nAlarm: CHARGING.")
CHARGED = production("charged",
                     r"Primary charge-delay interval elapses.",
                     r"Alarm: UP\nBus: Enable AC outlets.")
MAINSOFF = production("mainsoff",
                      r"Mains power is off.",
                      r"Bus: Poll of midbrain reveals bad voltage\nAlarm: DOWN")
MAINSDROP = production("mainsdrop",
                       r"Mains drop.",
                       r"Bus: Poll of midbrain reveals bad voltage")
RESTORED = production("restored",
                      r"Power restored.",
                      r"Bus: Poll of midbrain reports good voltage.")
DWELLTIMEOUT = production("dwelltimeout",
                    r"Dwell limit approaching.",
                    r"Bus: Hindbrain reports RemainingTimeAlarm().\nAlarm: SOS\nDisplay: 'Shutdown'\nShip shutdown command to USB-monitoring host.")
AWAITDOWN = production("awaitdown",
                       r"Mains power is off",
                       r"Bus: Poll of midbrain reveals bad voltage")
AVERTED = production("averted",
                     r"Secondary charge-delay interval elapses.",
                     r"Alarm: UP\nBus: Cycle AC power to load host via ENABLE.")

if __name__ == '__main__':
    import sys
    if len(sys.argv) > 1:
        render = firmware()
    else:
        render = dot()

    # This is the pseudocode describing state-action transitions.
    render.begin()
    render.state("midbrain_up", r"Initial power on")
    render.action("midbrain_up", "daemon_up", FOREBRAINUP) 
    render.action("midbrain_up", "midbrain_up", NOCONFIG)
    render.state("daemon_up", r"Daemon running.") 
    render.action("daemon_up", "charge_wait", CHARGING)
    render.state("charge_wait", r"Charge wait")
    render.action("charge_wait", "mains_up", CHARGED)
    render.action("charge_wait", "on_battery", MAINSDROP)
    render.state("mains_up", r"On mains power")
    render.action("daemon_up", "on_battery", MAINSOFF)
    render.state("on_battery", r"On battery power.")
    render.action("mains_up", "on_battery", MAINSDROP)
    render.action("on_battery", "pre_shutdown", DWELLTIMEOUT)
    render.state("pre_shutdown", r"Awaiting power drop")
    render.action("pre_shutdown", "shutdown", AWAITDOWN)
    render.state("shutdown", r"Shutdown")
    render.action("on_battery", "charge_wait", RESTORED)
    render.action("shutdown", "charge_wait", RESTORED)
    render.action("pre_shutdown", "mains_up", AVERTED)
    render.end()

# end

